//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include <cenvir.h>
#include <cmessage.h>
#include <cobjectfactory.h>
#include <cpar.h>
#include <cregistrationlist.h>
#include <csimplemodule.h>
#include <csimulation.h>
#include <Consumer.h>
#include <ConsumerMessage_m.h>
#include <regmacros.h>
#include <simtime.h>
#include <simutil.h>

namespace producerconsumer {

Define_Module(Consumer);

void Consumer::initialize()
{
    servedRequestRate = registerSignal("servedRequestRate");
    averageServingTime = registerSignal("averageServingTime");
    scheduleNewRequest();
    startSimulation = simTime();
    count = 0;
}

void Consumer::scheduleNewRequest() {
    ConsumerMessage *msg = new ConsumerMessage("Serveme");
    msg->setPacketSize(intuniform(par("minPacketSize"), par("maxPacketSize")));
    scheduleAt(simTime() + truncnormal(par("transTimeMean").doubleValue(), par("transTimeStdev").doubleValue()), msg);
}

void Consumer::handleMessage(cMessage *msg)
{
    if (msg->isSelfMessage()) {
        send(msg, "out");
        scheduleNewRequest();
    }
    else {
        ev << "Message arrived at " << msg->getArrivalGate() << "\n";
        emit(averageServingTime, simTime() - msg->getCreationTime());
        count++;
        delete msg;
    }
}

void Consumer::finish(){
    simtime_t endSimulation = simTime();
    emit(servedRequestRate, count / (endSimulation - startSimulation));
}

} //namespace
