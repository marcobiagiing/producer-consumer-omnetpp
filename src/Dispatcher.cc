//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include <cobjectfactory.h>
#include <cownedobject.h>
#include <cregistrationlist.h>
#include <csimplemodule.h>
#include <ConsumerMessage_m.h>
#include <Dispatcher.h>
#include <regmacros.h>
#include <simutil.h>

namespace producerconsumer {

Define_Module(Dispatcher);

void Dispatcher::initialize()
{
    servedRequestRateTotal = registerSignal("servedRequestRateTotal");
    averageServingTimeTotal = registerSignal("averageServingTimeTotal");
    startSimulation = simTime();
    count = 0;
}

void Dispatcher::handleMessage(cMessage *msg)
{
    ConsumerMessage *consumer_msg = (ConsumerMessage*) msg;
    emit(averageServingTimeTotal, simTime() - msg->getCreationTime());
    count++;
    send(consumer_msg, "out", consumer_msg->getGateField());
}

void Dispatcher::finish(){
    simtime_t endSimulation = simTime();
    emit(servedRequestRateTotal, count / (endSimulation - startSimulation));
}

} //namespace
