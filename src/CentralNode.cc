//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include <cobjectfactory.h>
#include <cqueue.h>
#include <cregistrationlist.h>
#include <csimplemodule.h>
#include <CentralNode.h>
#include <ConsumerMessage_m.h>
#include <regmacros.h>
#include <simutil.h>

namespace producerconsumer {

Define_Module(CentralNode);

void CentralNode::initialize()
{
    q_a = new cQueue();
    q_b = new cQueue();
    producerFree = false;

    if(par("fairPolicy").boolValue())
        ev<<"Using fair policy";
    else
        ev<<"Using Max rate policy";
}

void CentralNode::handleMessage(cMessage *msg)
{
    if(msg->arrivedOn("in",0)) {
        // arrived from consumer A
        ConsumerMessage *queue_msg = check_and_cast<ConsumerMessage*>(msg);
        queue_msg->setGateField(0);
        q_a->insert(msg);
    } else if (msg->arrivedOn("in", 1)) {
        // arrived from consumer B
        ConsumerMessage *queue_msg = check_and_cast<ConsumerMessage*>(msg);
        queue_msg->setGateField(1);
        q_b->insert(msg);
    } else if (msg->arrivedOn("inputProducer")) {
        // producer is free
        producerFree = true;
        delete msg;
    }

    if (producerFree == true) {
        if(par("fairPolicy").boolValue())
            sendWithFairPolicy();
        else
            sendWithMaxRatePolicy();
    }
}

//Always win the smallest head-of-line packet
void CentralNode::sendWithMaxRatePolicy(){
    if(q_a->length() == 0 && q_b->length() == 0)
        return;

    if(q_a->length() == 0){//Only queue b contains some messages
        cMessage *new_msg = (cMessage*)q_b->pop();
        send(new_msg, "out");
    }else if(q_b->length() == 0){//Only queue a contains some messages
        cMessage *new_msg = (cMessage*)q_a->pop();
        send(new_msg, "out");
    }else{//Both queue contains messages
        ConsumerMessage *new_msg_a = (ConsumerMessage*)q_a->front();
        ConsumerMessage *new_msg_b = (ConsumerMessage*)q_b->front();

        //message in queue b is smaller
        if(new_msg_a->getPacketSize() > new_msg_b->getPacketSize()) {
            send((ConsumerMessage*)q_b->pop(), "out");
        } else {
            //message in queue a is smaller
            send((ConsumerMessage*)q_a->pop(), "out");
        }
    }
    producerFree = false;

}

//Win the older packet
void CentralNode::sendWithFairPolicy(){
    if(q_a->length() == 0 && q_b->length() == 0)
        return;

    if(q_a->length() == 0){//Only queue b contains some messages
        cMessage *new_msg = (cMessage*)q_b->pop();
        send(new_msg, "out");
    }else if(q_b->length() == 0){//Only queue a contains some messages
        cMessage *new_msg = (cMessage*)q_a->pop();
        send(new_msg, "out");
    }else{//Both queue contains messages
        cMessage *new_msg_a = (cMessage*)q_a->front();
        cMessage *new_msg_b = (cMessage*)q_b->front();

        if(new_msg_a->getCreationTime() > new_msg_b->getCreationTime())//message in queue b is older
            send((ConsumerMessage*)q_b->pop(), "out");
        else//message in queue a is older
            send((ConsumerMessage*)q_a->pop(), "out");
    }
    producerFree = false;
}

void CentralNode::finish() {
    q_a->clear();
    q_b->clear();
    delete q_a;
    delete q_b;
}

} //namespace
