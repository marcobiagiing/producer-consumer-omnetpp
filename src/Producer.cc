//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include <cmessage.h>
#include <cobjectfactory.h>
#include <cownedobject.h>
#include <cregistrationlist.h>
#include <csimplemodule.h>
#include <csimulation.h>
#include <ConsumerMessage_m.h>
#include <distrib.h>
#include <Producer.h>
#include <regmacros.h>
#include <simtime.h>
#include <simutil.h>

namespace producerconsumer {

Define_Module(Producer);

void Producer::initialize()
{
    sendFreeMessage();
}

void Producer::handleMessage(cMessage *msg)
{
    if (msg->isSelfMessage()) {
        send(msg, "out");
        sendFreeMessage();
    } else {
        ConsumerMessage *consumer_msg = (ConsumerMessage*)msg;
        scheduleAt(simTime() + consumer_msg->getPacketSize(), msg);
    }
}

void Producer::sendFreeMessage() {
    cMessage *msg = new cMessage("free");
    send(msg, "outputCentralNode");
}

} //namespace
